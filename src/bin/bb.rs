use std::path::PathBuf;

use bitbucket_rs::*;
use clap;
use config;
use directories::ProjectDirs;

pub fn main() {
    let matches = clap::App::new("Bitbucket CLI")
        .version("0.1")
        .author("Antoine Busch")
        .about("Command-line tool for Bitbucket")
        .subcommand(clap::SubCommand::with_name("repositories").about("Query repositories"))
        .subcommand(clap::SubCommand::with_name("pr").about("Query pull requests"))
        .subcommand(
            clap::SubCommand::with_name("pipelines")
                .about("Query pipelines")
                .arg(clap::Arg::with_name("REPO").required(true).index(1)),
        )
        .get_matches();

    let project_dirs =
        ProjectDirs::from("org", "Bitbucket", "bbcli").expect("Missing config directory");

    let config_dir = project_dirs.config_dir();
    let mut config_file = PathBuf::from(config_dir);
    config_file.push("config");

    let mut config = config::Config::default();
    config
        .merge(config::File::with_name(config_file.to_str().unwrap()))
        .expect("Failed to load config file");

    let username = config
        .get_str("user")
        .expect("Missing `user` field in configuration file");
    let password = config
        .get_str("password")
        .expect("Missing `password` field in configuration file");

    let bb = Bitbucket::new(Credentials::new(username.to_string(), password.to_string()));

    if let Some(_matches) = matches.subcommand_matches("repositories") {
        let repos = bb.repositories().unwrap();
        for repo in repos.values {
            println!("{:?}", repo);
        }
    } else if let Some(_matches) = matches.subcommand_matches("pr") {
        let prs = bb.pull_requests().unwrap();
        for pr in prs.values {
            println!("{:?}", pr);
        }
    } else if let Some(matches) = matches.subcommand_matches("pipelines") {
        let repo = matches.value_of("REPO").unwrap();
        let pipelines = bb.pipelines(repo).unwrap();
        for pipeline in pipelines.values {
            println!("{:?}", pipeline);
        }
    } else {
        eprintln!("Command not implemented!");
    };
}
