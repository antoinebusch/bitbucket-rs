use std::error::Error;

use reqwest::{Client, RequestBuilder};
use serde::{Deserialize, Serialize};
use serde_json::Value;

const API_BASE: &str = "https://api.bitbucket.org/2.0";

pub struct Bitbucket {
    client: Client,
    creds: Credentials,
}

pub struct Credentials {
    pub username: String,
    pub password: String,
}

impl Credentials {
    pub fn new(username: String, password: String) -> Self {
        Self { username, password }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Paged<T> {
    pub pagelen: u32,
    pub page: u32,
    pub size: u32,
    pub values: Vec<T>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Repository {
    pub full_name: String,
    pub description: String,
    pub is_private: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PullRequest {
    pub title: String,
    pub summary: RichText,
    pub state: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RichText {
    pub raw: String,
    pub markup: String,
    pub html: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct User {
    pub account_id: String,
    pub nickname: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PipelineState {
    pub name: String,
    pub result: PipelineResult,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PipelineResult {
    pub name: String,
}

// #[derive(Debug, Serialize, Deserialize)]
// pub struct Commit {

#[derive(Debug, Serialize, Deserialize)]
pub struct Pipeline {
    pub uuid: String,
    pub build_number: u32,
    pub creator: User,
    pub state: PipelineState,
    // pub target: Commit,
}

impl Bitbucket {
    pub fn new(creds: Credentials) -> Self {
        Self {
            client: Client::new(),
            creds,
        }
    }

    pub fn repositories(&self) -> Result<Paged<Repository>, Box<dyn Error>> {
        let rb = self.client.get(&format!(
            "{}/users/{}/repositories",
            API_BASE, self.creds.username
        ));
        let data = self.sign(rb).send()?.text()?;

        let page = serde_json::from_str(&data)?;

        Ok(page)
    }

    pub fn pull_requests(&self) -> Result<Paged<PullRequest>, Box<dyn Error>> {
        let rb = self.client.get(&format!(
            "{}/pullrequests/{}",
            API_BASE, self.creds.username
        ));
        let data = self.sign(rb).send()?.text()?;

        let page = serde_json::from_str(&data)?;

        Ok(page)
    }

    pub fn pipelines(&self, repo_name: &str) -> Result<Paged<Pipeline>, Box<dyn Error>> {
        let url = format!("{}/repositories/{}/pipelines/", API_BASE, repo_name);
        dbg!(&url);
        let rb = self.client.get(&url);
        let data = self.sign(rb).send()?.text()?;

        let page = serde_json::from_str(&data)?;

        Ok(page)
    }

    fn sign(&self, request_builder: RequestBuilder) -> RequestBuilder {
        request_builder.basic_auth(&self.creds.username, Some(self.creds.password.clone()))
    }
}
